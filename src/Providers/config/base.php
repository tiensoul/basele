<?php

return [

    'directory' => [
        'path' => env('DATASOURCE_PATH', 'Sources')
    ],

    'provider' => [
        'file' => env('PROVIDER_NAME', 'RepositoryServiceProvider')
    ],
];
